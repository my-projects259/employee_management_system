package com.example.employeemanagementsystem.controllers;

import com.example.employeemanagementsystem.entities.Employee;
import com.example.employeemanagementsystem.services.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/employees")
public class EmployeeRestController {

  final EmployeeService employeeService;

  public EmployeeRestController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @PostMapping("/save")
  public ResponseEntity save(@RequestBody Employee employee) {
    return employeeService.save(employee);
  }

  @GetMapping("/list")
  public ResponseEntity list() {
    return employeeService.list();
  }

  @DeleteMapping("/delete")
  public ResponseEntity delete( @RequestParam String id ) {
    return employeeService.delete(id);
  }

  @PutMapping("/update")
  public ResponseEntity update( @RequestBody Employee employee ) {
    return employeeService.update(employee);
  }
}