package com.example.employeemanagementsystem.repositories;

import com.example.employeemanagementsystem.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}