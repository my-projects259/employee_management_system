package com.example.employeemanagementsystem.services;


import com.example.employeemanagementsystem.entities.Employee;
import com.example.employeemanagementsystem.repositories.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class EmployeeService {

  final EmployeeRepository employeeRepository;

  public EmployeeService(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  public ResponseEntity save(Employee employee) {
    Map<String, Object> hm = new HashMap<>();
    Employee e = employeeRepository.save(employee);
    hm.put("employees", employee);
    return new ResponseEntity(hm, HttpStatus.OK);
  }

  public ResponseEntity list() {
    Map<String, Object> hm = new HashMap<>();
    hm.put("employees", employeeRepository.findAll());
    return new ResponseEntity(hm, HttpStatus.OK);
  }

  public ResponseEntity delete(String id) {
    Map<String, Object> hm = new HashMap<>();
    try {
      int iid = Integer.parseInt(id);
      employeeRepository.deleteById(iid);
      hm.put("status", true);
    } catch (Exception ex) {
      hm.put("message", "id request : " + id);
      hm.put("status", false);
      return new ResponseEntity(hm, HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity(hm, HttpStatus.OK);
  }

  public ResponseEntity update(Employee employee) {
    Map<String, Object> hm = new HashMap<>();
    Optional<Employee> oUser = employeeRepository.findById(employee.getId());
    if (oUser.isPresent()) {
      employeeRepository.saveAndFlush(employee);
      hm.put("message", employee);
      hm.put("status", true);
    } else {
      hm.put("message", "Fail uid");
      hm.put("status", false);
    }
    return new ResponseEntity(hm, HttpStatus.OK);
  }
}



